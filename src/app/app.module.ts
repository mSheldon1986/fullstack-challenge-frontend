import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';

import { AppRoutingModule } from './app-routing.module';
import { AppComponent } from './app.component';
import { AllListsComponent } from './all-lists/all-lists.component';
import { SingleListComponent } from './single-list/single-list.component';

@NgModule({
  declarations: [
    AppComponent,
    AllListsComponent,
    SingleListComponent
  ],
  imports: [
    BrowserModule,
    AppRoutingModule
  ],
  providers: [],
  bootstrap: [AppComponent]
})
export class AppModule { }
