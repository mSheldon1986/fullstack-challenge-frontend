import { Component, OnInit } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { Router } from '@angular/router';
import { Observable } from 'rxjs';
import { GroceryListModule } from '../Models/grocery-list/grocery-list.module';

@Component({
  selector: 'app-all-lists',
  templateUrl: './all-lists.component.html',
  styleUrls: ['./all-lists.component.css']
})
export class AllListsComponent implements OnInit {


  constructor(private router:Router,GroceryLists:Observable<GroceryListModule>,private httpClient: HttpClient) { 
    
  }

  ngOnInit(): void {
    
  }
  viewList(id:number):void{
    
  }
  addItemToList(id:number):void{

  }

  deleteList(id:number):void{
    
  }
}